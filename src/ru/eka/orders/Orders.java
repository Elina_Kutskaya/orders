package ru.eka.orders;

import java.util.Arrays;
import java.util.Collections;

/**
 * Класс для подсчета максимально заработанной
 * суммы
 *
 * @author Куцкая Э.А.,15ИТ18
 */
public class Orders {
    public static void main(String[] args) {
        int time = 4;
        Integer[] arr = {35, 9, 29, 17, 46, 8, 44};
        Arrays.sort(arr, Collections.reverseOrder());
        System.out.println("Максимально заработанная сумма денег: " + summation(time, arr) + "$");
    }

    /**
     * Метод суммирования наивигодных заказов
     *
     * @param time доступное время работы
     * @param arr  массив стоимости заказов
     * @return заработанную сумму денег
     */
    private static int summation(int time, Integer[] arr) {
        int sum = 0;
        if (arr.length < time) {
            for (int i = 0; i < arr.length; i++)
            sum += arr[i];
        } else {
            for (int i = 0; i < time; i++) {
                sum += arr[i];
            }
        }
        return sum;
    }
}
